from django.shortcuts import render


def home_page(request):
    return render(request, "home.html", {"title": "Home", "my_list": [1, 2, 3, 4, 5]})


def about_page(request):
    return render(request, "about.html", {"title": "About"})


def contact_page(request):
    return render(request, "contact.html", {"title": "Contact"})
